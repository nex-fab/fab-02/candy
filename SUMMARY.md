# Summary
  
## Tutorials
* [1.Project manage](https://git-scm.com/)
    * [Introducemyself](1PROJECTMANAGEMENT/Introduce_Myself.md)
    * [My final project](1PROJECTMANAGEMENT/Final_Project.md)


*  [2. CAD design](doc/2cad/cad.md)
   

* [3. 3Dprinter](doc/33dprinter/assignment.md)
  * [practice](33dprinting/practice.md) 

* [4. Electric design ](doc/4electric_design_and_manfucture/basicknowledge.md)
  

* [5. Arduino application](doc/../5Arduino/assignment.md)
    * [Practice](5Arduino/practic.md)
  
   
* [6. Laser cutting](doc/../6LaserCutting/assignment.md)
    * [Box_Practice](6LaserCutting/box_practice.md)
  
  
* [7. PCB manufacture](https://www.nexpcb.com/blog/smt-pcb-puzzle)
 
		
* [8. CNC manufacture](https://astromachineworks.com/what-is-cnc-machining/)


*  [9. IOT and Interaction](https://en.wikipedia.org/wiki/Internet_of_Things)
  

