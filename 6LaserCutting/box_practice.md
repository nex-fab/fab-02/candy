# BOX Drawing <> AutoCAD

On Tuesday afternoon, we start the lesson about Laser topic. After Bob introduce the Laser theory, we start drawing. I record the steps below:
## Step 1
Open the AutoCAD software, then we need to initial the line types first. In general, in one drawing, there are at least 4 kinds of lines, which includes
* Thin solid line
* Thick solid line
* dotted line
* Stippled line

So, we need to check if it has load these 4 lines. Typing 'L'to call the line setting window out, and you need to choose the line you need to add and click 'Load' and then 'Add'. Like below:
![](https://gitlab.com/candypic/candyprojectpic/uploads/a8cb1acd02286711a81508c2ad7c7d44/20200826171415.png)
## Step 2
Drawing a rectangle with 90*90. Typing 'rec' and fill in your length.
![](https://gitlab.com/candypic/candyprojectpic/uploads/60e18215c40191ca80eb9fc3a94e8b77/20200826171718.png)
![](https://gitlab.com/candypic/candyprojectpic/uploads/8c8a3bb45088b76e2706fa8d4ad7823a/20200826171737.png)
The inner rectangle is the same steps.
## Step 3
Add the Centerline, then Trim the extra line ‘tr’. Finally, the shape we need is this.
![](https://gitlab.com/candypic/candyprojectpic/uploads/03350dfadf2657a896109cf5cdc60ccf/20200826172258.png)

## Additional: How to add a good dimension？
First, typing 'di' and confirm the 2 points that need to measured, and pull and click.
![](https://gitlab.com/candypic/candyprojectpic/uploads/b40d99fe80ade8922c0932be15efbfc7/20200826172805.png)
You can see the arrow and the digital are very small. So, we need to set the size of the arrow and text. Typing 'd', and click setting shape and choose 'Modify'.
![](https://gitlab.com/candypic/candyprojectpic/uploads/ab6e7fbfe92e9c58794deefdac93826d/20200826173214.png)
Amend the text height and arrow size.
![](https://gitlab.com/candypic/candyprojectpic/uploads/08656720e2ddd6e4dcca9133963a2eee/20200826173301.png)
![](https://gitlab.com/candypic/candyprojectpic/uploads/8545594e7b9989507b94e25af70c8cb6/20200826173340.png)
Then, fianlly we complete.
![](https://gitlab.com/candypic/candyprojectpic/uploads/c7ce2b4620a429379a90ae48b432ea19/20200826173422.png)

---TED---