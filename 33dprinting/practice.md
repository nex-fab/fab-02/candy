# 3D Printing of Charger

![](https://gitlab.com/candypic/candyprojectpic/uploads/1c8d1349e906e3c7bf264da25b8be87e/20200818133921.jpeg)
It is a very interesting stuff, though we failed in the 1st time. But we are patient and we success in the 2nd time!

Below are the main steps for the Charger 3D printing:
* We need to finish the 3D modeling.
* Import the 3D modeling file to the Cura software to setting the parameters to fit the 3D printing. (thickness of top, botttom, support, wall thickness...)
* Then, we move to the 3D machine to import the 3D printing file.
* And then, it starts....

We are so excited it works! This is me and my teammate's first 3D printing product, we are happy to see it.

Below is sharing the Charge deformation process:
![](https://gitlab.com/candypic/candyprojectpic/uploads/62f65d549a88d3ea0f7ca05d577fc6f6/20200824211633.jpeg)
|
![](https://gitlab.com/candypic/candyprojectpic/uploads/8c819a337e40da12bb9001c389a38eb0/20200824211746.jpeg)
|
![](https://gitlab.com/candypic/candyprojectpic/uploads/2c849a5330a8a69f55d07d6dee99a09a/20200824211838.jpeg)
|
![](https://gitlab.com/candypic/candyprojectpic/uploads/20cdf4d029345aafed2209e6bfcb512f/20200824212115.jpeg)
|
![](https://gitlab.com/candypic/candyprojectpic/uploads/bac0678d6267b1e1d3e22d06ff431375/20200824212427.jpeg)
|
![](https://gitlab.com/candypic/candyprojectpic/uploads/201ac5eabda0f15d6e025c2bf9ac7393/20200824212444.jpeg)

Finally, we need to move the Charger from the platform, remove the supports, and make the surface more smooth. THEN, COMPLETED!!
![](https://gitlab.com/candypic/candyprojectpic/uploads/37b22eee3334f154ffa9cf07f234e9d3/20200824212754.jpeg)
![](https://gitlab.com/candypic/candyprojectpic/uploads/296db755e61c736cb1ce6d9c36512335/20200824212807.jpeg)

----END----